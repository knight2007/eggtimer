package main;

import javax.microedition.lcdui.*;
import javax.microedition.rms.*;
import javax.microedition.media.*;
import javax.microedition.media.control.*;
import java.io.*;
import java.util.*;
import javax.microedition.rms.*;

public class MainCanvas extends Canvas implements Runnable {
    
    EggTimer mid;
    int screen_width;
    int screen_height;
    int mode;
    int selected;
    String current_time;
    int[] time_numbers;
    long current_time_milli;
    long start_timestamp;
    int selected_preset;
    
    Random generator;
    
    final static int MODE_SETUP = 0;
    final static int MODE_RUNNING = 1;
    final static int MODE_RINGING = 2;
    final static int MODE_PRESETS_TYPE = 3;
    final static int MODE_PRESETS_MODE = 4;
    
    public MainCanvas(EggTimer m) {
        mid = m;
        this.setFullScreenMode(true);
        new Thread(this).start();
        
        screen_width = this.getWidth();
        screen_height = this.getHeight();
        
        time_numbers = new int[4];
        setCurrentTime();
        initImages();
        generator = new Random(System.currentTimeMillis());
        mode = MODE_SETUP;
        selected = 0;
    }
    
    Image back;
    Image numbers;
    Image font;
    
    int numberFontWidths[];
    int numberFontOffsets[];
    int numberFontCodes[];
    
    int fontData[] = new int[]
    {
        0, 0, 0, 0, 4,
        0, 0, 4, 16, 3,
        4, 0, 4, 16, 4,
        8, 0, 10, 16, 10,
        18, 0, 7, 16, 7,
        25, 0, 9, 16, 9,
        34, 0, 9, 16, 8,
        43, 0, 2, 16, 2,
        45, 0, 4, 16, 4,
        49, 0, 4, 16, 4,
        53, 0, 6, 16, 6,
        59, 0, 11, 16, 10,
        70, 0, 3, 16, 3,
        73, 0, 5, 16, 5,
        78, 0, 3, 16, 3,
        81, 0, 5, 16, 4,
        86, 0, 8, 16, 8,
        94, 0, 4, 16, 4,
        98, 0, 7, 16, 6,
        105, 0, 7, 16, 7,
        112, 0, 8, 16, 8,
        120, 0, 7, 16, 6,
        127, 0, 7, 16, 7,
        134, 0, 6, 16, 6,
        140, 0, 7, 16, 7,
        147, 0, 7, 16, 7,
        154, 0, 3, 16, 3,
        157, 0, 3, 16, 3,
        160, 0, 11, 16, 10,
        171, 0, 11, 16, 10,
        182, 0, 11, 16, 10,
        193, 0, 6, 16, 6,
        199, 0, 13, 16, 13,
        212, 0, 8, 16, 8,
        220, 0, 8, 16, 8,
        228, 0, 8, 16, 7,
        236, 0, 9, 16, 9,
        245, 0, 6, 16, 6,
        0, 16, 6, 16, 6,
        6, 16, 9, 16, 9,
        15, 16, 10, 16, 9,
        25, 16, 4, 16, 3,
        29, 16, 6, 16, 5,
        35, 16, 8, 16, 8,
        43, 16, 6, 16, 6,
        49, 16, 11, 16, 11,
        60, 16, 10, 16, 9,
        70, 16, 10, 16, 9,
        80, 16, 8, 16, 7,
        88, 16, 10, 16, 9,
        98, 16, 8, 16, 7,
        106, 16, 7, 16, 7,
        113, 16, 6, 16, 6,
        119, 16, 9, 16, 9,
        128, 16, 7, 16, 7,
        135, 16, 12, 16, 11,
        147, 16, 7, 16, 7,
        154, 16, 7, 16, 6,
        161, 16, 7, 16, 6,
        168, 16, 4, 16, 4,
        172, 16, 5, 16, 4,
        177, 16, 4, 16, 4,
        181, 16, 13, 16, 13,
        194, 16, 6, 16, 6,
        200, 16, 6, 16, 6,
        206, 16, 8, 16, 7,
        214, 16, 8, 16, 7,
        222, 16, 6, 16, 6,
        228, 16, 8, 16, 7,
        236, 16, 7, 16, 7,
        243, 16, 4, 16, 4,
        247, 16, 8, 16, 7,
        0, 32, 8, 16, 7,
        8, 32, 3, 16, 3,
        11, 32, 3, 16, 3,
        14, 32, 7, 16, 6,
        21, 32, 3, 16, 3,
        24, 32, 11, 16, 11,
        35, 32, 8, 16, 7,
        43, 32, 7, 16, 7,
        50, 32, 8, 16, 7,
        58, 32, 8, 16, 7,
        66, 32, 5, 16, 4,
        71, 32, 5, 16, 5,
        76, 32, 4, 16, 4,
        80, 32, 7, 16, 7,
        87, 32, 6, 16, 6,
        93, 32, 10, 16, 9,
        103, 32, 6, 16, 6,
        109, 32, 6, 16, 6,
        115, 32, 6, 16, 5,
        121, 32, 6, 16, 6,
        127, 32, 6, 16, 6,
        133, 32, 6, 16, 6,
        139, 32, 11, 16, 10
    };
    
    int[] fontCodes = new int[]
    {
        0x0020,
        0x0021,
        0x0022,
        0x0023,
        0x0024,
        0x0025,
        0x0026,
        0x0027,
        0x0028,
        0x0029,
        0x002a,
        0x002b,
        0x002c,
        0x002d,
        0x002e,
        0x002f,
        0x0030,
        0x0031,
        0x0032,
        0x0033,
        0x0034,
        0x0035,
        0x0036,
        0x0037,
        0x0038,
        0x0039,
        0x003a,
        0x003b,
        0x003c,
        0x003d,
        0x003e,
        0x003f,
        0x0040,
        0x0041,
        0x0042,
        0x0043,
        0x0044,
        0x0045,
        0x0046,
        0x0047,
        0x0048,
        0x0049,
        0x004a,
        0x004b,
        0x004c,
        0x004d,
        0x004e,
        0x004f,
        0x0050,
        0x0051,
        0x0052,
        0x0053,
        0x0054,
        0x0055,
        0x0056,
        0x0057,
        0x0058,
        0x0059,
        0x005a,
        0x005b,
        0x005c,
        0x005d,
        0x005e,
        0x005f,
        0x0060,
        0x0061,
        0x0062,
        0x0063,
        0x0064,
        0x0065,
        0x0066,
        0x0067,
        0x0068,
        0x0069,
        0x006a,
        0x006b,
        0x006c,
        0x006d,
        0x006e,
        0x006f,
        0x0070,
        0x0071,
        0x0072,
        0x0073,
        0x0074,
        0x0075,
        0x0076,
        0x0077,
        0x0078,
        0x0079,
        0x007a,
        0x007b,
        0x007c,
        0x007d,
        0x007e
    };
    
    int[] fontKerning = new int[]
    {
        70, 44, -1,
        70, 46, -1,
        76, 86, -1,
        76, 89, -1,
        80, 44, -2,
        80, 46, -2,
        84, 44, -1,
        84, 45, -1,
        84, 46, -1,
        84, 97, -1,
        84, 99, -1,
        84, 101, -1,
        84, 111, -1,
        84, 115, -1,
        86, 44, -1,
        86, 46, -1,
        87, 44, -1,
        87, 46, -1,
        89, 44, -2,
        89, 45, -1,
        89, 46, -2,
        89, 58, -1,
        89, 59, -1,
        89, 97, -1,
        89, 101, -1,
        89, 111, -1,
        114, 44, -1,
        114, 46, -1
    };
    
    int[] fontCodeTable;
    
    public void initImages() {
        try{
            back = Image.createImage("/back.png");
            numbers = Image.createImage("/numbers.png");
            font = Image.createImage("/font.png");
            numberFontWidths = new int[]
            {
                17,10,15,15,18,15,16,15,15,16,8
            };
            
            int offset = 0;
            numberFontOffsets = new int[numberFontWidths.length];
            for(int i=0;i<numberFontWidths.length;i++) {
                numberFontOffsets[i] = offset;
                offset+=numberFontWidths[i];
            }
            
            numberFontCodes = new int[256];
            numberFontCodes[(int) "0".charAt(0)] = 0;
            numberFontCodes[(int) "1".charAt(0)] = 1;
            numberFontCodes[(int) "2".charAt(0)] = 2;
            numberFontCodes[(int) "3".charAt(0)] = 3;
            numberFontCodes[(int) "4".charAt(0)] = 4;
            numberFontCodes[(int) "5".charAt(0)] = 5;
            numberFontCodes[(int) "6".charAt(0)] = 6;
            numberFontCodes[(int) "7".charAt(0)] = 7;
            numberFontCodes[(int) "8".charAt(0)] = 8;
            numberFontCodes[(int) "9".charAt(0)] = 9;
            numberFontCodes[(int) ":".charAt(0)] = 10;
            
            fontCodeTable = new int[256];
            for(int i=0;i<fontCodes.length;i++)
                fontCodeTable[fontCodes[i]] = i;
            
            
            
        } catch(Exception e) {
            
        }
    }
    
    public void setCurrentTime() {
        current_time = "";
        for(int i=0;i<4;i++) {
            current_time = current_time + Integer.toString(time_numbers[i]);
            if(i==1)current_time = current_time + ":";
        }
    }
    
    public void run() {
        while(true) {
            update();
            
            this.repaint();
            long nexttime = System.currentTimeMillis() + 30; // set your framerate here
            while(System.currentTimeMillis() < nexttime)Thread.yield();
        }
    }
    
    int blinker;
    
    public void update() {
        switch(mode) {
            case MODE_SETUP:
            case MODE_PRESETS_TYPE:
            case MODE_PRESETS_MODE:
                blinker++;
                break;
            case MODE_RUNNING:
                int left = (int) (current_time_milli - (System.currentTimeMillis() - start_timestamp));
                if(left<=0) {
                    playMidi();
                    mode = MODE_RINGING;
                }
                int minutes = left / (60*1000);
                int seconds = (left-minutes*60*1000)/1000;
                String min_string = ""+minutes;
                if(min_string.length()==1)min_string = "0"+min_string;
                String sec_string = ""+seconds;
                if(sec_string.length()==1)sec_string = "0"+sec_string;
                current_time = min_string+":"+sec_string;
                break;
            case MODE_RINGING:
                blinker++;
                shake_x = (generator.nextInt()%5);
                shake_y = (generator.nextInt()%5);
                break;
        }
    }
    
    
    
    public void keyPressed(int key) {
        if(mode == MODE_SETUP) {
            int number_pressed = -1;
            if(key == KEY_NUM0)
                number_pressed = 0;
            if(key == KEY_NUM1)
                number_pressed = 1;
            if(key == KEY_NUM2)
                number_pressed = 2;
            if(key == KEY_NUM3)
                number_pressed = 3;
            if(key == KEY_NUM4)
                number_pressed = 4;
            if(key == KEY_NUM5)
                number_pressed = 5;
            if(key == KEY_NUM6 & selected != 2)
                number_pressed = 6;
            if(key == KEY_NUM7 & selected != 2)
                number_pressed = 7;
            if(key == KEY_NUM8 & selected != 2)
                number_pressed = 8;
            if(key == KEY_NUM9& selected != 2)
                number_pressed = 9;
            
            if(number_pressed>-1 & selected < 4) {
                time_numbers[selected] = number_pressed;
                selected++;
            } else {
                int action = getGameAction(key);
                if(action == UP) {
                    if(selected<4) {
                        if(selected==2)
                            time_numbers[2] = (time_numbers[2]+1)%6;
                        else
                            time_numbers[selected] = (time_numbers[selected]+1)%10;
                    } else {
                        if(selected>4)
                            selected--;
                        else
                            selected=0;
                    }
                }
                if(action == DOWN) {
                    if(selected<4) {
                        int new_val = time_numbers[selected]-1;
                        if(selected==2) {
                            if(new_val==-1)
                                new_val=5;
                        } else {
                            if(new_val==-1)
                                new_val=9;
                        }
                        time_numbers[selected] = new_val;
                    } else {
                        selected++;
                        if(selected>6)
                            selected = 6;
                    }
                }
                if(action == FIRE) {
                    if(selected<4)
                        selected++;
                    else {
                        if(selected==4) {
                            start_timestamp = System.currentTimeMillis();
                            
                            int minutes = ((time_numbers[0]*10) + time_numbers[1]);
                            int seconds = ((time_numbers[2]*10) + time_numbers[3])+1;
                            current_time_milli = (minutes * 60 * 1000) + (seconds * 1000);
                            mode = MODE_RUNNING;
                        }
                        if(selected==5) {
                            selected=0;
                            mode = MODE_PRESETS_TYPE;
                        }
                        if(selected==6) {
                            mid.notifyDestroyed();
                        }
                    }
                }
                if(action == LEFT & selected>0) {
                    selected--;
                }
                if(action == RIGHT & selected<6)
                    selected++;
                
            }
        } else if(mode==MODE_PRESETS_TYPE) {
            int action = getGameAction(key);
            if(action == UP && selected>0)
                selected--;
            if(action == DOWN && selected<2)
                selected++;
            if(action == FIRE) {
                selected_preset = selected;
                selected = 0;
                mode = MODE_PRESETS_MODE;
            }
        } else if(mode==MODE_PRESETS_MODE)
        {
            int action = getGameAction(key);
            if(action == UP && selected>0)
                selected--;
            if(action == DOWN && selected<1)
                selected++;
            if(action == FIRE) {
                int[] presets = new int[]
                {
                    0,3,0,0,
                    0,4,3,0,
                    0,5,3,0,
                    0,5,0,0,
                    0,6,5,0,
                    0,8,0,0,
                };
                int offset = selected * 12 + selected_preset * 4;

                time_numbers[0]=presets[offset++];
                time_numbers[1]=presets[offset++];
                time_numbers[2]=presets[offset++];
                time_numbers[3]=presets[offset++];
                
                mode = MODE_SETUP;
                selected = 4;
            }            
        }
        
        else {
            int action = getGameAction(key);
            if(action==FIRE)
            {
                mode = MODE_SETUP;
                stopMidi();
            }
        }
        
        if(mode != MODE_RUNNING)
            setCurrentTime();
    }
    
    public void keyReleased(int key) {
        
    }
    
    int shake_x;
    int shake_y;
    
    public void paint(Graphics g) {
        int color = 0x548b91;
        if(mode == MODE_RINGING && (blinker%10>5))
            color = 0xffffff;
        
        
        g.setColor(color);
        g.fillRect(0,0,screen_width,screen_height);
        
        int x = (screen_width-128)>>1;
        int y = (screen_height-128)>>1;
        g.drawImage(back,x+shake_x,y+shake_y,0);
        
        
        if(mode != MODE_PRESETS_TYPE && mode != MODE_PRESETS_MODE)
            paintNumbersCentered(g);
        
        switch(mode) {
            case MODE_SETUP:
                y = (screen_height>>1)+10;
                if(selected!=4 | (blinker%20>10)) {
                    paintTextCentered(g,"START",y);
                }
                y+=15;
                if(selected!=5 | (blinker%20>10)) {
                    paintTextCentered(g,"PRESETS",y);
                }
                y+=15;
                if(selected!=6 | (blinker%20>10)) {
                    paintTextCentered(g,"EXIT",y);
                }
                break;
            case MODE_PRESETS_TYPE:
                y = (screen_height>>1)-10;
                if(selected!=0 | (blinker%20>10)) {
                    paintTextCentered(g,"Supreme Runny",y);
                }
                y+=20;
                if(selected!=1 | (blinker%20>10)) {
                    paintTextCentered(g,"Perfect Plasma",y);
                }
                y+=20;
                if(selected!=2 | (blinker%20>10)) {
                    paintTextCentered(g,"Hard Boiled",y);
                }
                break;
            case MODE_PRESETS_MODE:
                y = (screen_height>>1)-15;
                if(selected!=0 | (blinker%20>10)) {
                    paintTextCentered(g,"Eggs in cold water",y);
                    y+=13;
                    paintTextCentered(g,"Start when water is",y);
                    y+=13;
                    paintTextCentered(g,"boiling",y);

                }
                y = (screen_height>>1)+30;
                if(selected!=1 | (blinker%20>10)) {
                    paintTextCentered(g,"Start when putting",y);
                    y+=13;
                    paintTextCentered(g,"eggs in boiling water",y);
                }
                break;
        }
        
    }
    
    public void paintNumbersCentered(Graphics g) {
        
        int width = 0;
        
        for(int i=0;i<current_time.length();i++) {
            int c = (int) current_time.charAt(i);
            width+=numberFontWidths[numberFontCodes[c]];
        }
        
        int x = (screen_width - width) >>1;
        int y = (screen_height >>1)-15;
        
        for(int i=0;i<current_time.length();i++) {
            int c = (int) current_time.charAt(i);
            int code = numberFontCodes[c];
            int char_w = numberFontWidths[code];
            int char_off = numberFontOffsets[code];
            
            int sel = selected;
            if(sel>1)sel++;
            
            if(sel!=i | (blinker%20>10)) {
                g.setClip(x+shake_x,y+shake_y,char_w,20);
                g.drawImage(numbers,x-char_off+shake_x,y+shake_y,0);
            }
            x+=char_w;
        }
        
    }
    
    public void paintTextCentered(Graphics g,String text,int y) {
        int width = 0;
        int textlen = text.length();
        for(int i=0;i<textlen;i++) {
            int code = fontCodeTable[(int)text.charAt(i)];
            int char_width = fontData[code*5+4];
            width+=char_width;
        }
        
        int x = (screen_width-width)>>1;
        int last_char = -1;
        
        for(int i=0;i<textlen;i++) {
            int this_char = (int)text.charAt(i);
            int code = fontCodeTable[this_char];
            int offset = code*5;
            int sx = fontData[offset++];
            int sy = fontData[offset++];
            int sw = fontData[offset++];
            int sh = fontData[offset++];
            int aw = fontData[offset++];
            
            if(i>0) {
                int koff = 0;
                int klen = fontKerning.length / 3;
                for(int k=0;k<klen;k++) {
                    koff = k*3;
                    if(last_char==fontKerning[koff] && this_char==fontKerning[koff+1]) {
                        x+=fontKerning[koff+2];
                        break;
                    }
                }
            }
            
            if(sw>0 && sh>0) {
                g.setClip(x,y,sw,sh);
                g.drawImage(font,x-sx,y-sy,0);
            }
            x+=aw;
            last_char = (int)text.charAt(i);
        }
        
    }
    
    Player player = null;
    
    public void stopMidi() {
        try{
            if(player!=null) {
                player.stop();
                player.deallocate();
            }
            player = null;
            System.gc();
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void playMidi() {
        try {
            stopMidi();
            player = Manager.createPlayer(this.getClass().getResourceAsStream("/ring.mid"),"audio/midi");
            player.realize();
            VolumeControl vc = (VolumeControl) player.getControl("VolumeControl");
            vc.setLevel(100);
            player.start();
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    protected void hideNotify() {
        
    }
    
    protected void showNotify() {
        
    }
}

