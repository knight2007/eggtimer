package main;

 
import javax.microedition.midlet.*;
import javax.microedition.lcdui.*;
 

public class EggTimer extends MIDlet {
    
    public EggTimer() {
    }
    
    public MainCanvas main;

    private void initialize() {
        main = new MainCanvas(this);
        this.getDisplay().setCurrent(main);

    }
    
    public Display getDisplay() { 
        return Display.getDisplay(this);
    }
    
    
    public void startApp() {
        initialize();
    }
    
    public void pauseApp() {
    }
    
    public void destroyApp(boolean unconditional) {
    	 notifyDestroyed();
    }
    
}
